from collections import defaultdict
from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import dump
import os
import pandas as pd

class Get_Recommendation:

    def __init__(self):
        pass

    def get_top_n(self, predictions, n):
        # First map the predictions to each user.
        top_n = defaultdict(list)
        for uid, iid, true_r, est, _ in predictions:
            top_n[uid].append((iid, est))

        # Then sort the predictions for each user and retrieve the k highest ones.
        for uid, user_ratings in top_n.items():
            user_ratings.sort(key=lambda x: x[1], reverse=True)
            top_n[uid] = user_ratings[:n]

        return top_n

    def get_results(self, userid):

        result = []

        # path to dataset file
        file_path = '/home/aliko/Desktop/FYP/TripRec/TripRec-Dataset/TripRec_ratings.csv'

        #get the csv file to dataframe
        df = pd.read_csv(file_path)

        # A reader is still needed but only the rating_scale param is requiered.
        reader = Reader(rating_scale=(1, 5))

        # The columns must correspond to user id, item id and ratings (in that order).
        data = Dataset.load_from_df(df[['user', 'item', 'rating']], reader)

        #train the dataset by applying the SVD algorithm
        trainset = data.build_full_trainset()

        file_name = os.path.expanduser("~/Desktop/model")

        _, loaded_algo = dump.load(file_name)

        # Then predict ratings for all pairs (u, i) that are NOT in the training set.
        not_rated = trainset.build_anti_testset()

        # set the testset to data for requested ID
        target = [val for val in not_rated if val[0] == userid]

        # set predictions for required user
        predictions = loaded_algo.test(target)

        # get top_n recommendation
        top_n = self.get_top_n(predictions, 5)

        # Print the recommended items for each user
        for uid, user_ratings in top_n.items():
            for (iid, _) in user_ratings:
                result.append(iid)

        return result