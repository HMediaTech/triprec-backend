import psycopg2
from CF import Get_Recommendation

# open connection
class Database:

    def Connect(self):
        conn = psycopg2.connect(database="triprec", user="ali", password="aliko1993",
        host="127.0.0.1", port="5432")
        return conn

    # function to find based on query
    def Find(self,query):
        connection = self.Connect()
        cursor = connection.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        connection.close()
        if not rows:
            return None
        else:
            return rows


    def Execute(self,query):
        connection = self.Connect()
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
        connection.close()

if __name__ == '__main__':
    gr =Get_Recommendation()
    uID = 14817
    gr.get_results(uID)
