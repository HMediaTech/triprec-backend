from flask import Flask, jsonify, request, Response
import json
import logging
import csv
import bcrypt
import jwt
import grequests
from datetime import datetime, timedelta
from CF import Get_Recommendation
from DB_Controller import Database
from operator import itemgetter
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

# log = logging.getLogger('werkzeug')
# log.setLevel(logging.ERROR)

# class instances
db = Database()

# store packages
packages = []
days = 0
times = 1


# method to check list objects
def contains(list, filter):
    for x in list:
        if filter(x):
            return False
    return True

# write results to ratings.csv file
def add_to_file(data):

    # open file and append data to the end of the file
    with open('/home/aliko/Desktop/FYP/TripRec/TripRec-Dataset/TripRec_ratings.csv', 'a') as csvfile:
        fieldnames = ['user','item','rating']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        for i in range(len(data["ratings"])):
           writer.writerow({'user': data["userID"], 'item': data["ratings"][i]["id"],
                            'rating': data["ratings"][i]["rate"]})

    return data["userID"]


@app.route('/register', methods=['POST'])
def Registration():
    # get the data to the format
    data = json.loads(request.data)

    var = data['fname'], data['lname'], data['email'], \
          bcrypt.hashpw(data['password'].encode('utf-8'), bcrypt.gensalt()), 1

    # check if email exits
    if db.Find("select * from users WHERE email='%s'" % data['email']) == None:
        db.Execute("insert into users(fname,lname,email,password,rated) VALUES ('%s','%s','%s','%s',%s)"%var)
        return 'You have successfully registered!'
    else:
        return Response("Sorry, this email already exists!", status=400)


@app.route('/login', methods=['POST'])
def Login():
    # get the data to the format
    data = json.loads(request.data)

    # check if email exits
    row = db.Find("select * from users WHERE email='%s'" % data['email'])
    if row == None:
        return Response("Sorry, user does not exist", status=400)
    elif bcrypt.hashpw(data['password'].encode('utf-8'), row[0][4]) == row[0][4]:
        # create payload
        payload = {
            'email':data['email'],
            'password':data['password'],
            'exp':1
        }
        # create token
        jwt_token = jwt.encode({'exp': datetime.now() + timedelta(hours=24)},'key', algorithm='HS256')

        # response object
        response = {'userID':row[0][0],
                    'username': row[0][1],
                    'token': jwt_token,
                    'email': row[0][3]}
        return jsonify(response)
    else:
        return Response("Sorry password is not correct", status=400)

@app.route('/hotels', methods=['POST'])
def GetHotels():

    data = json.loads(request.data)
    userID = data['userID']

    # get the rated value from users table
    rated = db.Find("select * from users WHERE ID =%s" % userID)

    try:
        # # verify token
        # jwt.decode(token, 'key', algorithms='HS256')
        # find rated value of the user
        row_rated = db.Find("select * from users WHERE ID =%s" % userID)

        # find start and end num of the rated value
        start = rated[0][5]
        end = start + 5
        var1 = start, end

        # find
        row_hotels = db.Find("select * from hotels WHERE hotelid BETWEEN %s and %s" % var1)

        # update rated value
        var2 = end+1, userID
        db.Execute("Update users set rated=%s where ID=%s" %var2)

        # add data to the response object
        response=[]

        for i in range(len(row_hotels)):
            # get data to the object
            data = {
                "address":row_hotels[i][0],
                "city":row_hotels[i][1],
                "country":row_hotels[i][2],
                "latitude":row_hotels[i][3],
                "longitude":row_hotels[i][4],
                "name":row_hotels[i][5],
                "hotelID":row_hotels[i][6],
                "price":row_hotels[i][7],
                "image":row_hotels[i][8],
                "rating":row_hotels[i][9],
                "stars":row_hotels[i][10]
            }

            # append data objectto the response object
            response.append(data)
        return jsonify(response)
    except:
        return "false"

@app.route('/test', methods=['POST'])
def test():
    gr = Get_Recommendation()
    return gr.get_results(14817)

@app.route('/budget', methods=['POST'])
def GetBudget():

        gr = Get_Recommendation()

        req_data = json.loads(request.data)

        # add requested data to ratings.csv
        uID = add_to_file(req_data["ratings_results"])

        # send userID to the recommendation engine
        result = gr.get_results(int(uID))

        # find hotels details for recommended hotels
        hotels = db.Find('select * from hotels WHERE hotelid IN (%s,%s,%s,%s,%s)' % tuple(result))

        # global variables declaration
        global days, times, packages

        # append list elements to locations list
        urls = []

        # packages list
        all_packages = []

        #response list
        response = {}

        # create url, and append to list
        for hotel in hotels:
            urls.append('https://developers.zomato.com/api/v2.1/search?'+'lat='+
                        str(hotel[3])+'&lon='+str(hotel[4])+'&radius='+str(req_data['radius'])
                        +'&cuisines='+str(req_data['cuisines']))

        # concurrent call to Zomato.com API
        data = (grequests.get(url,
                              headers= {'user-key': 'ed61f3deaf8a4a72a575c1bf41636fb4'})
              for url in urls)

        # aggreagate restauranst data and hotel data
        for restaurants, hotel in zip(grequests.map(data), hotels):

            # rest&hotel
            rest_hotel = {"hotel": {}, "restaurants": []}

            # hotel info
            hotel_info = {}
            hotel_info['country'] = hotel[2]
            hotel_info['city'] = hotel[1]
            hotel_info['name'] = hotel[5]
            hotel_info['price'] = hotel[7]
            hotel_info['image'] = hotel[8]
            hotel_info['rating'] = hotel[9]
            hotel_info['stars'] = hotel[10]

            for restaurant in restaurants.json()['restaurants']:

                # aggregate restaurant data
                agg_restaurant = {"user_ratings": {}, "location": {}}

                agg_restaurant['name'] = restaurant['restaurant']['name']
                agg_restaurant['cuisines'] = restaurant['restaurant']['cuisines']
                agg_restaurant['cost'] = restaurant['restaurant']['average_cost_for_two']/2
                agg_restaurant['user_ratings']['agg_rating'] = restaurant['restaurant']['user_rating']['aggregate_rating']
                agg_restaurant['user_ratings']['rating_color'] = restaurant['restaurant']['user_rating']['rating_color']
                agg_restaurant['user_ratings']['rating_text'] = restaurant['restaurant']['user_rating']['rating_text']
                agg_restaurant['location']['address'] = restaurant['restaurant']['location']['address']
                agg_restaurant['location']['city'] = restaurant['restaurant']['location']['city']
                agg_restaurant['location']['lat'] = restaurant['restaurant']['location']['latitude']
                agg_restaurant['location']['lon'] = restaurant['restaurant']['location']['longitude']

                # append data to the list
                rest_hotel['restaurants'].append(agg_restaurant)

            rest_hotel['hotel'] = hotel_info
            all_packages.append(rest_hotel)

        # calculations for budgets
        # hotels list
        all_hotels = []
        for package in all_packages:
            all_hotels.append(package["hotel"]["price"])

        # restaurants list
        all_restaurants = []
        for package in all_packages:
            for restaurant in package['restaurants']:
                if restaurant['cost']>0:
                  all_restaurants.append(restaurant['cost'])

        # Max budget
        max_budget = (max(all_hotels)+(max(all_restaurants)*req_data['times']))*req_data['days']

        # Min budget
        min_budget = (min(all_hotels)+(min(all_restaurants)*req_data['times']))*req_data['days']

        # Avg buget
        avg_budget = (min_budget+max_budget)/2

        response['Max'] = max_budget
        response['Min'] = min_budget
        response['Avg'] = avg_budget
        response['First'] = (min_budget+avg_budget)/2
        response['Third'] = (max_budget+avg_budget)/2

        #store all_packages, days, and times
        packages[:] = []
        packages.extend(all_packages)
        days = req_data['days']
        times = req_data['times']

        print "max hotels"
        print max(all_hotels)
        print "max restaurants"
        print max(all_restaurants)
        print response
        return jsonify(response)

@app.route('/packages', methods=['POST'])
def GetPackages():

    # get requested budget
    data = json.loads(request.data)
    budget = data['budget']

    # response object
    response = []

    # calculate daily restaurant budget for each package
    for package in packages:

        rest_hotel = {"hotel": {}, "restaurants": {}}

        temp_budget = budget
        rest_budget = (temp_budget - (package['hotel']['price']*days))/days

        if (rest_budget>0):
            # set hotels data
            rest_hotel["hotel"] = package["hotel"]

            # set restaurants data
            restaurants = []
            restaurants = package["restaurants"]
            if times == 2:
                all_comb = []
                combinations = []
                accepted = []
                id=0
                for i in xrange(0, len(restaurants)):
                    for j in xrange(i, len(restaurants)):
                        if ((restaurants[i]['name'] != restaurants[j]['name']) and
                                restaurants[i]['cost']!=0 and restaurants[j]['cost']!=0 and
                                (restaurants[i]['cost'] + restaurants[j]['cost'] < rest_budget)):

                            price = float(restaurants[i]['cost'])+\
                                     float(restaurants[j]['cost'])
                            id+=1
                            all_comb.append({"id": id, "comb":[restaurants[i], restaurants[j]],
                                             "price":price})
                            # get all the accepted restaurants, and compare with all restaurants
                            if contains(accepted, lambda x: x['name']==restaurants[i]['name']):
                                  accepted.append(restaurants[i])
                            if contains(accepted, lambda x: x['name']==restaurants[j]['name']):
                                  accepted.append(restaurants[j])
                id=0
                # find not accepted restaurants list
                accepted_rests = set((x['name']) for x in accepted)
                others = [x for x in restaurants if (x['name']) not in accepted_rests and (x['cost'])>0]

                # to find the best ones from all combinations
                for restaurant in accepted:

                    temp_list = []
                    for comb in all_comb:
                        if comb['comb'][0]['name'] == restaurant['name']:
                            temp_list.append(comb)

                    if len(temp_list)!=0:
                       #  find maximum priced combination from list
                       temp_object = max(temp_list, key=itemgetter("price"))

                       # check if final combination contains that combination
                       if contains(combinations, lambda x:x['id']==temp_object['id']):
                          combinations.append(temp_object)

                       # delete combinations that contains same restaurant as temp_object
                       for comb in list(all_comb):
                           if comb['comb'][1]['name'] == temp_object['comb'][1]['name']:
                                all_comb.remove(comb)

                combinations.sort(key=lambda x: x['price'], reverse=True)

                rest_hotel['restaurants']['combinations'] = combinations
                rest_hotel['restaurants']['others'] = others
                rest_hotel['times'] = 2
                rest_hotel['rest_budget'] = rest_budget

                # append new created package to the response list
                if rest_hotel['restaurants']['combinations']:
                    response.append(rest_hotel)
            else:
                matches = []
                others = []
                for restaurant in package['restaurants']:
                    if restaurant['cost'] <= rest_budget and restaurant['cost']!=0:
                        matches.append(restaurant)
                    elif restaurant['cost']!=0:
                        others.append(restaurant)
                rest_hotel["restaurants"]['matches'] = matches
                rest_hotel["restaurants"]['others'] = others
                rest_hotel['times'] = 1
                rest_hotel['rest_budget'] = rest_budget

                # append new created package to the response list
                if rest_hotel["restaurants"]["matches"]:
                   response.append(rest_hotel)

    return jsonify(response)


if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000, threaded = True)